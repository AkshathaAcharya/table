import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import reducer from "./store/reducer";
import { Provider } from "react-redux";
import { createStore,applyMiddleware } from "redux";
import rootsaga from "./store/saga"
import createSagaMiddleware from "redux-saga";
const sagaMiddleware = createSagaMiddleware()

const store = createStore(reducer, applyMiddleware(sagaMiddleware))
sagaMiddleware.run(rootsaga);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider >,
  document.getElementById('root')
);


reportWebVitals();
