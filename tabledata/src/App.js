
import './App.css';
import Users from './pages/users';
import StickyHeadTable from "./pages/table"
import Token from "./pages/token"
import {BrowserRouter, Redirect, Route, Switch} from"react-router-dom"

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={Users} />
          <Route path="/vehicle" exact component={StickyHeadTable} />
           <Route path="/token" exact component={Token}/>
          <Redirect to="/"/>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
