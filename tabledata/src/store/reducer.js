const i = {
    storeApi: [],
    storeuser: [],
    storeToken:[]
}

const reducer = (state = i, action) => {
    var newState = { ...state }
    if (action.type === "STORE") {
        return {
            ...newState,
            storeApi:action.payload
        }
    }
      if (action.type === "STORE_USER") {
        return {
            ...newState,
            storeuser:action.payload
          }
          
    }
      if (action.type === "TOKEN_STORE") {
        return {
            ...newState,
            storeToken:action.payload
          }
          
    }
    return newState
}
export default reducer