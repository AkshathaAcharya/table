import {takeLatest,call,put,all} from "redux-saga/effects"
import axios from "axios";

function fetchFromaxios() {
   return axios.get("https://findfalcone.herokuapp.com/vehicles")
}
function* fetchdata() {
    var res = yield call(fetchFromaxios)
    yield put({type:"STORE", payload:res.data})
    
}
function* vehicles() {
     yield takeLatest("FETCH",fetchdata)
}

function fechaxiosforuser() {
   return axios.get("https://findfalcone.herokuapp.com/planets")
}
function* fetchUser() {
    var res = yield call(fechaxiosforuser)
    yield put({type:"STORE_USER", payload:res.data})
    
}
function* users() {
     yield takeLatest("FETCH_USERS",fetchUser)
}



function* fetchtoken() {

 yield   axios.post("https://findfalcone.herokuapp.com/token",  {
     headers: {
          
           ' Accept' : 'application/json'
        }
  })
        .then((response) => {
        console.log(response)
      put({type:"TOKEN_STORE", payload:response.data})
  })
  .catch((error) => {
   console.log(error)
  })
    
}
function* token() {
     yield takeLatest("TOKEN",fetchtoken)
}

export default function* rootsaga() {
    yield all([vehicles(), users(),token()])
   
}